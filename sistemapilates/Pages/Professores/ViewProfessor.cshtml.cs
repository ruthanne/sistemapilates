﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using SistemaPilates.Models;

namespace SistemaPilates.Pages.Professores
{
    public class ViewProfessorModel : PageModel
    {
        private readonly SistemaPilates.Models.SistemaPilatesContext _context;

        public ViewProfessorModel(SistemaPilates.Models.SistemaPilatesContext context)
        {
            _context = context;
        }

        public IList<Professor> Professor { get;set; }

        public async Task OnGetAsync()
        {
            Professor = await _context.Professor
                .Include(p => p.Turma).ToListAsync();
        }
    }
}
