﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using SistemaPilates.Models;

namespace SistemaPilates.Pages.Professores
{
    public class CreateModel : PageModel
    {
        private readonly SistemaPilates.Models.SistemaPilatesContext _context;

        public CreateModel(SistemaPilates.Models.SistemaPilatesContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            //cria a lista de turmas com ID e Dias
            ViewData["Turma"] = new SelectList(_context.Turma, "Id", "Dias");

            return Page();
        }

        [BindProperty]
        public Professor Professor { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Professor.Add(Professor);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}