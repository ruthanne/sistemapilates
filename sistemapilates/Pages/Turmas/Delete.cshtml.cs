﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using SistemaPilates.Models;

namespace SistemaPilates.Pages.Turmas
{
    public class DeleteModel : PageModel
    {
        private readonly SistemaPilates.Models.SistemaPilatesContext _context;

        public DeleteModel(SistemaPilates.Models.SistemaPilatesContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Turma Turma { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Turma = await _context.Turma.FirstOrDefaultAsync(m => m.Id == id);

            if (Turma == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Turma = await _context.Turma.FindAsync(id);

            if (Turma != null)
            {
                _context.Turma.Remove(Turma);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
