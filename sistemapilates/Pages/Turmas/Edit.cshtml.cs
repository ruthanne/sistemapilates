﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaPilates.Models;

namespace SistemaPilates.Pages.Turmas
{
    public class EditModel : PageModel
    {
        private readonly SistemaPilates.Models.SistemaPilatesContext _context;

        public EditModel(SistemaPilates.Models.SistemaPilatesContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Turma Turma { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {

            ViewData["Turma"] = new SelectList(_context.Professor, "Id", "Nome");
            
            if (id == null)
            {
                return NotFound();
            }

            Turma = await _context.Turma.FirstOrDefaultAsync(m => m.Id == id);

            if (Turma == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Turma).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TurmaExists(Turma.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool TurmaExists(int id)
        {
            return _context.Turma.Any(e => e.Id == id);
        }
    }
}
