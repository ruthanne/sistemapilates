﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using SistemaPilates.Models;

namespace SistemaPilates.Pages.Turmas
{
    public class DetailsModel : PageModel
    {
        private readonly SistemaPilates.Models.SistemaPilatesContext _context;

        public DetailsModel(SistemaPilates.Models.SistemaPilatesContext context)
        {
            _context = context;
        }

        public Turma Turma { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Turma = await _context.Turma.FirstOrDefaultAsync(m => m.Id == id);

            //Professor.Turma = _context.Turma.Where(x => x.Id == Professor.TurmaId).FirstOrDefault();

            if (Turma == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
