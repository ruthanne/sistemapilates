﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using SistemaPilates.Models;

namespace SistemaPilates.Pages.Turmas
{
    public class CreateModel : PageModel
    {
        private readonly SistemaPilates.Models.SistemaPilatesContext _context;

        public CreateModel(SistemaPilates.Models.SistemaPilatesContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            //cria a lista de turmas com ID&Professores
            ViewData["Turma"] = new SelectList(_context.Professor, "Id", "Nome");
            return Page();
        }

        [BindProperty]
        public Turma Turma { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Turma.Add(Turma);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}