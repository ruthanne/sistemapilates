﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SistemaPilates.Models;

namespace SistemaPilates.Pages.Alunos
{
    public class DetailsModel : PageModel
    {
        private readonly SistemaPilates.Models.SistemaPilatesContext _context;

        public DetailsModel(SistemaPilates.Models.SistemaPilatesContext context)
        {
            _context = context;
        }

        public Aluno Aluno { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }

            Aluno = await _context.Aluno.FirstOrDefaultAsync(m => m.Id == id);


            Aluno.Turma = _context.Turma.Where(x => x.Id == Aluno.TurmaId).FirstOrDefault();
            

            if (Aluno == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
