﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using SistemaPilates.Models;

namespace SistemaPilates.Pages.Alunos
{
    public class CreateModel : PageModel
    {
        private readonly SistemaPilates.Models.SistemaPilatesContext _context;

        public CreateModel(SistemaPilates.Models.SistemaPilatesContext context)
        {
            _context = context;
        }

        public  IActionResult OnGet()
        {
           //cria a lista de turmas com ID&Dias
            ViewData["Turma"] = new SelectList(_context.Turma, "Id", "Dias");

            return Page();
        }

        [BindProperty]
        public Aluno Aluno { get; set; }

        public async Task<IActionResult> OnPostAsync(string Aluno_Turma)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }
            //verificando se foi selecionado uma turma
            if (Aluno_Turma != "")
            {
                //pegando a turma selecionada do banco e adicionando em aluno
                Aluno.Turma = _context.Turma.Where(x => x.Id ==  Convert.ToInt32(Aluno_Turma)).FirstOrDefault();
            }
            _context.Aluno.Add(Aluno);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}