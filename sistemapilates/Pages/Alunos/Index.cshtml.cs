﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using SistemaPilates.Models;

namespace SistemaPilates.Pages.Alunos
{
    public class IndexModel : PageModel
    {
        private readonly SistemaPilates.Models.SistemaPilatesContext _context;

        public IndexModel(SistemaPilates.Models.SistemaPilatesContext context)
        {
            _context = context;
        }

        public IList<Aluno> Aluno { get;set; }

        public async Task OnGetAsync()
        {
            Aluno = await _context.Aluno.ToListAsync();
            foreach(var aluno in Aluno)
            {
                aluno.Turma = _context.Turma.Where(x => x.Id == aluno.TurmaId).FirstOrDefault();
            }
            
        }
    }
}
