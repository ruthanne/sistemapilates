﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace SistemaPilates.Pages
{
    public class IndexModel : PageModel
    {

        public string Message { get; set; }
        public void OnGet()
        {
            Message = "Seu corpo é seu maior bem. Ele guarda e reflete a sua alma. Cuide dele como se fosse uma pedra preciosa e nós o lapidaremos.";
        }
    }
}
