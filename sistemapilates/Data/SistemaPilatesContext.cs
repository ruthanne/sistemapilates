﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SistemaPilates.Models;

namespace SistemaPilates.Models
{
    //Esta classe representa o contexto das nossas entidades mapeadas para as tabelas do banco de dados.
    public class SistemaPilatesContext : DbContext
    {
        public SistemaPilatesContext (DbContextOptions<SistemaPilatesContext> options)
            : base(options)
        {
        }

        public DbSet<SistemaPilates.Models.Aluno> Aluno { get; set; }

        public DbSet<SistemaPilates.Models.Turma> Turma { get; set; }

        public DbSet<SistemaPilates.Models.Professor> Professor { get; set; }
    }
}
