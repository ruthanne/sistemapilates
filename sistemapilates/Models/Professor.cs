﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SistemaPilates.Models
{
    [Table("Professor")]
    public class Professor
    {

        [Key]
        public int Id { get; set; }

        [Column("cpf_professor")]
        [Display(Name = "CPF")]
        [RegularExpression("(^[0-9]+)", ErrorMessage = "Por favor, insira somente números.")]
        [StringLength(11)]
        [Required(ErrorMessage = "Este campo é obrigatório.", AllowEmptyStrings = false)]
        public string Cpf { get; set; }

        [Column("nome")]
        [Display(Name = "Nome")]
        [RegularExpression("(^[a-zA-ZÀ-ú_ ]+$)", ErrorMessage = "Por favor, insira somente letras.")]
        [StringLength(50)]
        [Required(ErrorMessage = "Este campo é obrigatório.", AllowEmptyStrings = false)]
        public string Nome { get; set; }

        [DataType(DataType.Date)]
        [Column("data_nascimento")]
        [Display(Name = "Data de Nascimento")]
        [Required(ErrorMessage = "Este campo é obrigatório.", AllowEmptyStrings = false)]
        public DateTime DataNascimento { get; set; }

        [Column("cidade")]
        [Display(Name = "Cidade")]
        [StringLength(50)]
        public string Cidade { get; set; }

        [Column("estado")]
        [Display(Name = "Estado")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Por favor, insira somente letras.")]
        [StringLength(2)]
        public string Estado { get; set; }

        [Column("telefone")]
        [Display(Name = "Telefone")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Por favor, insira somente números.")]
        public string Telefone { get; set; }

        [Column("email")]
        [Display(Name = "Email")]
        [StringLength(50)]
        public string Email { get; set; }

        [Column("turma")]
        [Display(Name = "Turma")]
        public virtual Turma Turma { get; set; }

        public int TurmaId { get; set; }

        public Professor()
        {
        }

        public Professor(string Cpf, string Nome, DateTime DataNascimento, string Cidade, string Estado, string Telefone, string Email, Turma Turma)
        {
            this.Cpf = Cpf;
            this.Nome = Nome;
            this.DataNascimento = DataNascimento;
            this.Cidade = Cidade;
            this.Estado = Estado;
            this.Telefone = Telefone;
            this.Email = Email;
            this.Turma = Turma;
        }

    }
}
