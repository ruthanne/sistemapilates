﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace SistemaPilates.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new SistemaPilatesContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<SistemaPilatesContext>>()))
            {
                // Procura por aluno
                if (context.Aluno.Any())
                {
                    return;
                }

                Turma turma1 = new Turma();
                turma1.Dias = Turma.DiasEnum.SEGUNDA;
                turma1.Horarios = TimeSpan.Parse("16:00");

                Turma turma2 = new Turma();
                turma2.Dias = Turma.DiasEnum.QUARTA;
                turma2.Horarios = TimeSpan.Parse("17:00");

                Professor prof1 = new Professor();
                prof1.Cpf = "2321313";
                prof1.Nome = "Jennifer";
                prof1.DataNascimento = DateTime.Parse("12/03/1987");
                prof1.Cidade = "Sao Paulo";
                prof1.Estado = "SP";
                prof1.Telefone = "9929292929";
                prof1.Email = "jenny@gmail.com";
                prof1.Turma = turma1;

                //Range de ALUNO
                context.Aluno.AddRange(
                    new Aluno
                    {
                        cpf = "92839281928",
                        nome = "Ruth",
                        dataNascimento = DateTime.Parse("26/11/1994"),
                        cidade = "Salvador",
                        estado = "BA",
                        telefone = "3029-1827",
                        email = "ruth@gmail.com",
                        altura = 175,
                        peso = 71,
                        observacoes = "Nenhuma.",
                        Turma = turma1
                    },

                    new Aluno
                    {
                        cpf = "348348249",
                        nome = "Ramon",
                        dataNascimento = DateTime.Parse("26/09/1994"),
                        cidade = "Salvador",
                        estado = "BA",
                        telefone = "91294918",
                        email = "ramon@gmail.com",
                        altura = 179,
                        peso = 72,
                        observacoes = "Nenhuma.",
                        Turma = turma2
                    }
                );

                //Range de TURMA
                context.Turma.AddRange(
                    new Turma
                    {
                        Dias = Turma.DiasEnum.SEGUNDA,
                        Horarios = TimeSpan.Parse("16:00")
                    },

                    new Turma
                    {
                        Dias = Turma.DiasEnum.TERCA,
                        Horarios = TimeSpan.Parse("17:00")
                    },

                    new Turma
                    {
                        Dias = Turma.DiasEnum.QUARTA,
                        Horarios = TimeSpan.Parse("16:00")
                    },

                    new Turma
                    {
                        Dias = Turma.DiasEnum.QUINTA,
                        Horarios = TimeSpan.Parse("17:00")
                    }
                );

                //Range de PROFESSOR
                context.Professor.AddRange(
                    new Professor
                    {
                        Cpf = "37283829123",
                        Nome = "Jennifer",
                        DataNascimento = DateTime.Parse("22/04/1979"),
                        Cidade = "Campinas",
                        Estado = "SP",
                        Telefone = "3981-9210",
                        Email = "jenny@gmail.com",
                        Turma = turma1
                    },

                    new Professor
                    {
                        Cpf = "28930192834",
                        Nome = "Carla",
                        DataNascimento = DateTime.Parse("17/11/1985"),
                        Cidade = "Salvador",
                        Estado = "BA",
                        Telefone = "3281-3421",
                        Email = "carla@gmail.com",
                        Turma = turma2
                    }
                );
                context.SaveChanges();
            }
        }
    }
}