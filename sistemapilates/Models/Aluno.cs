﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SistemaPilates.Models
{
    [Table("Aluno")]
    public class Aluno
    {
        [Key]
        public int Id { get; set; }

        [Column("cpf_aluno")]
        [Display(Name = "CPF")]
        [RegularExpression("(^[0-9]+)", ErrorMessage = "Por favor, insira somente números.")]
        [StringLength(11)]
        [Required(ErrorMessage = "Este campo é obrigatório.", AllowEmptyStrings = false)]
        public string cpf { get; set; }

        [Column("nome")]
        [Display(Name = "Nome")]
        [RegularExpression("(^[a-zA-ZÀ-ú_ ]+$)", ErrorMessage = "Por favor, insira somente letras.")]
        [StringLength(50)]
        [Required(ErrorMessage = "Este campo é obrigatório.", AllowEmptyStrings = false)]
        public string nome { get; set; }

        [DataType(DataType.Date)]
        [Column("data_nascimento")]
        [Display(Name = "Data de Nascimento")]
        [Required(ErrorMessage = "Este campo é obrigatório.", AllowEmptyStrings = false)]
        public DateTime dataNascimento { get; set; }

        [Column("cidade")]
        [Display(Name = "Cidade")]
        [StringLength(50)]
        public string cidade { get; set; }

        [Column("estado")]
        [Display(Name = "Estado")]
        [RegularExpression("^[a-zA-Z]+$", ErrorMessage = "Por favor, insira somente letras.")]
        [StringLength(2)]
        public string estado { get; set; }

        [Column("telefone")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Por favor, insira somente números.")]
        [Display(Name = "Telefone")]
        public string telefone { get; set; }

        [Column("email")]
        [Display(Name = "Email")]
        public string email { get; set; }

        [Column("altura")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Por favor, insira somente números.")]
        [Display(Name = "Altura")]
        [Required(ErrorMessage = "Este campo é obrigatório.", AllowEmptyStrings = false)]
        public float altura { get; set; }

        [Column("peso")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Por favor, insira somente números.")]
        [Display(Name = "Peso")]
        [Required(ErrorMessage = "Este campo é obrigatório.", AllowEmptyStrings = false)]
        public float peso { get; set; }

        [Column("observações")]
        [Display(Name = "Observações")]
        [StringLength(240)]
        public string observacoes { get; set; }

        [Column("turma")]
        [Display(Name = "Turma")]
        public Turma Turma { get; set; }

        [Column("turma_id")]
        public int TurmaId { get; set; }

    }
}
