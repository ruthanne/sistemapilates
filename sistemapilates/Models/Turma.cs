﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SistemaPilates.Models
{
    [Table("Turma")]
    public class Turma
    {
        [Key]
        [Column("id_turma")]
        public int Id { get; set; }

        [Column("dias_turma")]
        [Display(Name = "Dias")]
        [Required(ErrorMessage = "Este campo é obrigatório.", AllowEmptyStrings = false)]
        public DiasEnum Dias { get; set; }

        [Column("horarios_turma")]
        [Display(Name = "Horários")]
        [Required(ErrorMessage = "Este campo é obrigatório.", AllowEmptyStrings = false)]
        public TimeSpan Horarios { get; set; }

        [Column("professores_turma")]
        [Display(Name = "Professor")]
        public List<Professor> Professor { get; set; }

        [Column("alunos_turma")]
        [Display(Name = "Alunos")]
        public List<Aluno> Alunos { get; set; }

        public enum DiasEnum
        {
            SEGUNDA,
            TERCA,
            QUARTA,
            QUINTA,
            SEXTA
        }


    }
}
