#pragma checksum "C:\Users\Anne\Documents\Solutis\SistemaPilates\SistemaPilates\Pages\Sobre.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "67e2e77c8961fa26f68fa8c779d8cf8c6624869b"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(SistemaPilates.Pages.Pages_Sobre), @"mvc.1.0.razor-page", @"/Pages/Sobre.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.RazorPages.Infrastructure.RazorPageAttribute(@"/Pages/Sobre.cshtml", typeof(SistemaPilates.Pages.Pages_Sobre), null)]
namespace SistemaPilates.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\Anne\Documents\Solutis\SistemaPilates\SistemaPilates\Pages\_ViewImports.cshtml"
using SistemaPilates;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"67e2e77c8961fa26f68fa8c779d8cf8c6624869b", @"/Pages/Sobre.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"9ef71d82bef7f1dcbe03a70513a53df2d32be812", @"/Pages/_ViewImports.cshtml")]
    public class Pages_Sobre : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "C:\Users\Anne\Documents\Solutis\SistemaPilates\SistemaPilates\Pages\Sobre.cshtml"
  
    ViewData["Title"] = "Sobre Estúdio Pilates";

#line default
#line hidden
            BeginContext(64, 4, true);
            WriteLiteral("<h1>");
            EndContext();
            BeginContext(69, 17, false);
#line 5 "C:\Users\Anne\Documents\Solutis\SistemaPilates\SistemaPilates\Pages\Sobre.cshtml"
Write(ViewData["Title"]);

#line default
#line hidden
            EndContext();
            BeginContext(86, 17, true);
            WriteLiteral("</h1>\r\n\r\n<html>\r\n");
            EndContext();
            BeginContext(103, 1814, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("body", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "67e2e77c8961fa26f68fa8c779d8cf8c6624869b3573", async() => {
                BeginContext(109, 1801, true);
                WriteLiteral(@"

    <style>
        .bar {
            padding-top: 10px;
             }
    </style>

    <div class=""text-left"">
        <h5 class=""text-black-50"">Pilates desenvolve um corpo uniforme, corrige posturas erradas, restaura a vitalidade física, vigora a mente e eleva o espírito.</h5>
        <img src=""https://www.purepilates.com.br/assets/images/bg-banner-call.jpg"" style=""width:1100px; height:383px;"" align=""middle"" />
    </div>

    <div class=""text-left, bar"">
        <h4>Informações de contato:</h4>
        <h6 class=""text-black-50"">Telefone: (71) 3000-0000</h6>
        <h6 class=""text-black-50"">Email: <a href=""mailto:estudiopilates@gmail.com""> estudiopilates@gmail.com</a></h6>
    </div>
    <div class=""text-left, bar"">
        <h4>Como nos localizar:</h4>
        <h6 class=""text-black-50"" >Endereço: Rua Arthur de Azevedo Machado, 317 - STIEP, Salvador/BA.</h6>
    </div>

    <style>
        #map {
            height: 400px;
            width: 850px;
            margin-left:150");
                WriteLiteral(@"px;
        }
    </style>

    <div class=""row"">
        <div id=""map""></div>
        <script>
            var map;
            function initMap() {
                map = new google.maps.Map(document.getElementById('map'), {
                    center: { lat: -12.997142, lng: -38.446825 },
                    zoom: 18
                });

                var marker = new google.maps.Marker({
                    position: { lat: -12.997142, lng: -38.446825 },
                    map : map
                });
            marker.setMap(map); 
            }
        </script>

        <script src=""https://maps.googleapis.com/maps/api/js?key=AIzaSyD7s-Mxm-6CsyHVxM7NRi1_2UNoaHkTz9Q&callback=initMap""
                async defer></script>
    </div>
");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1917, 9, true);
            WriteLiteral("\r\n</html>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Pages_Sobre> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<Pages_Sobre> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<Pages_Sobre>)PageContext?.ViewData;
        public Pages_Sobre Model => ViewData.Model;
    }
}
#pragma warning restore 1591
