﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace SistemaPilates.Migrations
{
    public partial class Pilates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Turma",
                columns: table => new
                {
                    id_turma = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    dias_turma = table.Column<int>(nullable: false),
                    horarios_turma = table.Column<TimeSpan>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Turma", x => x.id_turma);
                });

            migrationBuilder.CreateTable(
                name: "Aluno",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    cpf_aluno = table.Column<string>(maxLength: 11, nullable: false),
                    nome = table.Column<string>(nullable: false),
                    data_nascimento = table.Column<DateTime>(nullable: false),
                    cidade = table.Column<string>(nullable: true),
                    estado = table.Column<string>(maxLength: 2, nullable: true),
                    telefone = table.Column<string>(nullable: true),
                    email = table.Column<string>(nullable: true),
                    altura = table.Column<float>(nullable: false),
                    peso = table.Column<float>(nullable: false),
                    observações = table.Column<string>(nullable: true),
                    turma_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Aluno", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Aluno_Turma_turma_id",
                        column: x => x.turma_id,
                        principalTable: "Turma",
                        principalColumn: "id_turma",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Professor",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    cpf_professor = table.Column<string>(maxLength: 11, nullable: false),
                    nome = table.Column<string>(nullable: false),
                    data_nascimento = table.Column<DateTime>(nullable: false),
                    cidade = table.Column<string>(maxLength: 40, nullable: true),
                    estado = table.Column<string>(maxLength: 2, nullable: true),
                    telefone = table.Column<string>(nullable: true),
                    email = table.Column<string>(maxLength: 50, nullable: true),
                    TurmaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Professor", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Professor_Turma_TurmaId",
                        column: x => x.TurmaId,
                        principalTable: "Turma",
                        principalColumn: "id_turma",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Aluno_turma_id",
                table: "Aluno",
                column: "turma_id");

            migrationBuilder.CreateIndex(
                name: "IX_Professor_TurmaId",
                table: "Professor",
                column: "TurmaId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Aluno");

            migrationBuilder.DropTable(
                name: "Professor");

            migrationBuilder.DropTable(
                name: "Turma");
        }
    }
}
